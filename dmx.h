/*
 * dmx.h
 *
 * Created: 25-12-2012 19:45:17
 *  Author: Robbert-Jan de Jager
 */ 


#ifndef _DMX_H_
#define _DMX_H_

#define CHANNELS 64

void dmx_init();
uint8_t dmx_canChangeData();

extern uint8_t dmxChannels[CHANNELS];


#endif /* DMX_H_ */